FROM maven:3.6.3-jdk-8

ARG CI_JOB_TOKEN
ARG NEXUS_DEPLOY_KEY

RUN apt-get update && \
    apt-get install -y --no-install-recommends git

RUN git clone -b master --depth 1 https://gitlab-ci-token:${CI_JOB_TOKEN}@gitlab.com/WalrusNetwork/infrastructure/config.git server/
RUN git clone -b master --depth 1 https://gitlab-ci-token:${CI_JOB_TOKEN}@gitlab.com/WalrusNetwork/maps/maps.git maps/
RUN git clone -b master --depth 1 https://gitlab-ci-token:${CI_JOB_TOKEN}@gitlab.com/WalrusNetwork/ux-ui/minecraft-ui.git
RUN git clone -b master --depth 1 https://gitlab-ci-token:${CI_JOB_TOKEN}@gitlab.com/WalrusNetwork/ux-ui/translations.git

COPY import.sh .
COPY settings.xml .
RUN ./import.sh $CI_JOB_TOKEN

ENV OPTS="-XX:+UnlockExperimentalVMOptions -XX:+UseCGroupMemoryLimitForHeap -XshowSettings:vm -XX:MaxRAMFraction=1 -XX:+AggressiveOpts -XX:+AlwaysPreTouch -XX:+OptimizeStringConcat -XX:+UseStringDeduplication -XX:+UseCompressedOops -XX:TargetSurvivorRatio=90 -XX:InitiatingHeapOccupancyPercent=10 -XX:+UseConcMarkSweepGC -XX:+UseParNewGC -XX:+CMSIncrementalPacing -XX:ParallelGCThreads=2 -XX:+DisableExplicitGC -XX:+UseAdaptiveGCBoundary -Xnoclassgc"

EXPOSE 25565/tcp

WORKDIR server
CMD java $OPTS -jar bukkit.jar
