#!/bin/bash

GIT_TOKEN=$1
SP_VERSION="1.8.8-R0.1-SNAPSHOT"
MVN_BASE_OPTS="-s ../settings.xml -Dtransitive=false"
MVN_PRIV_OPTS="$MVN_BASE_OPTS -DrepositoryId=walrus-private -DgroupId=network.walrus -Dversion=1.0.0-SNAPSHOT"
MVN_PUB_OPTS="$MVN_BASE_OPTS -DrepositoryId=walrus=public -DgroupId=network.walrus -Dversion=$SP_VERSION"

pull_plugin () {
    mvn dependency:get $MVN_PRIV_OPTS -DartifactId=$1
    mvn dependency:copy $MVN_BASE_OPTS -DrepositoryId=walrus-private -Dartifact="network.walrus:$1:1.0.0-SNAPSHOT" -DoutputDirectory=plugins/
}

pull_component () {
    mvn dependency:get $MVN_PRIV_OPTS -DartifactId=$1
    mvn dependency:copy $MVN_BASE_OPTS -DrepositoryId=walrus-private -Dartifact="network.walrus:$1:1.0.0-SNAPSHOT" -DoutputDirectory=plugins/GameManager/components/
}

cd server/

mvn dependency:get $MVN_PUB_OPTS -DartifactId=sportpaper
mvn dependency:copy $MVN_BASE_OPTS -DrepositoryId=walrus-public -Dartifact="network.walrus:sportpaper:$SP_VERSION" -DoutputDirectory=.
mv sportpaper-*.jar bukkit.jar

pull_plugin "nerve-bukkit"
pull_plugin "ubiquitous-bukkit"
pull_plugin "games-core"

mkdir -p plugins/GameManager/components/
pull_component "games-octc"
